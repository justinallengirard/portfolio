import React from "react";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { ArrowDownCircleFill as DownIcon } from "react-bootstrap-icons";
import Button from "react-bootstrap/Button";
import ResumePDF from "../files/Justin-Girard-Resume.pdf";

export default function Resume(props) {
  return (
    <Row id="resume" style={{ flexGrow: 1 }}>
      <Col>
        <Row style={{ justifyContent: "center" }}>
          <h1 style={{ textAlign: "center", margin: 50 }}>RESUME</h1>
        </Row>
        <Row style={{ justifyContent: "center" }}>
          <Button
            variant="dark"
            onClick={() => {
              window.open(ResumePDF, "_blank");
            }}
          >
            View Resume
          </Button>
        </Row>
        <Row style={{ flexGrow: 0, justifyContent: "center" }}>
          <a href="#contact">
            <DownIcon size={50} color="#2e2e2e" style={{ margin: 50 }} />
          </a>
        </Row>
      </Col>
    </Row>
  );
}
