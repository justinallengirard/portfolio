import React from "react";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { ArrowDownCircleFill as DownIcon } from "react-bootstrap-icons";
import Card from "react-bootstrap/Card";
import ListGroup from "react-bootstrap/ListGroup";
import ListGroupItem from "react-bootstrap/ListGroupItem";
import Button from "react-bootstrap/Button";
import ReactIcon from "../images/react-small.png";
import PythonIcon from "../images/python-small.png";
import PostgreSQLIcon from "../images/postgresql-small.png";
import AWSIcon from "../images/aws-small.png";
import MaterialUIIcon from "../images/materialui-small.png";

export default function Projects(props) {
  return (
    <Row id="projects" style={{ flexGrow: 1, backgroundColor: "#2e2e2e" }}>
      <Col>
        <Row style={{ justifyContent: "center" }}>
          <h1 style={{ color: "white", textAlign: "center", margin: 50 }}>PROJECTS</h1>
        </Row>
        <Row style={{ justifyContent: "space-around" }}>
          <Card style={{ width: "20rem", marginTop: 10 }}>
            <Card.Body>
              <Card.Title>Shipment Log</Card.Title>
              <Card.Text style={{ height: 170 }}>
                An application that lets users add shipments, delete shipments, and view the path the carrier will take to deliver the shipment. This was my
                first experience building a React application. This was built to learn FreightVerify's tech stack.
              </Card.Text>
            </Card.Body>
            <ListGroup className="list-group-flush">
              <ListGroupItem>
                <span>
                  <img src={ReactIcon} alt="react" /> React
                </span>
              </ListGroupItem>
              <ListGroupItem>
                <span>
                  <img src={PythonIcon} alt="python" /> Python
                </span>
              </ListGroupItem>
              <ListGroupItem>
                <span>
                  <img src={PostgreSQLIcon} alt="postgreSQL" /> PostgreSQL
                </span>
              </ListGroupItem>
              <ListGroupItem>
                <span>
                  <img src={AWSIcon} alt="aws" /> AWS
                </span>
              </ListGroupItem>
            </ListGroup>
            <Card.Body>
              <Button
                variant="dark"
                style={{ width: "100%" }}
                onClick={() => {
                  window.open("https://www.shipmentlog.girard.codes", "_blank");
                }}
              >
                View
              </Button>
            </Card.Body>
          </Card>
          <Card style={{ width: "20rem", marginTop: 10 }}>
            <Card.Body>
              <Card.Title>MyTV</Card.Title>
              <Card.Text style={{ height: 170 }}>
                A customizable video player that allows you to add youtube videos to channels. The Random channel plays a random video from one of the other
                channels. A great application for background noise while developing.
              </Card.Text>
            </Card.Body>
            <ListGroup className="list-group-flush">
              <ListGroupItem>
                <span>
                  <img src={ReactIcon} alt="react" /> React
                </span>
              </ListGroupItem>
              <ListGroupItem>
                <span>
                  <img src={PythonIcon} alt="python" /> Python
                </span>
              </ListGroupItem>
              <ListGroupItem>
                <span>
                  <img src={PostgreSQLIcon} alt="postgreSQL" /> PostgreSQL
                </span>
              </ListGroupItem>
              <ListGroupItem>
                <span>
                  <img src={AWSIcon} alt="aws" /> AWS
                </span>
              </ListGroupItem>
            </ListGroup>
            <Card.Body>
              <Button
                variant="dark"
                style={{ width: "100%" }}
                onClick={() => {
                  window.open("https://www.mytv.girard.codes", "_blank");
                }}
              >
                View
              </Button>
            </Card.Body>
          </Card>
          <Card style={{ width: "20rem", marginTop: 10 }}>
            <Card.Body>
              <Card.Title>Steam Game Library</Card.Title>
              <Card.Text style={{ height: 170 }}>
                A simple application that retrieves the entire Steam game library found at store.steampowered.com/games/. This project was an opportunity to
                introduce myself to AWS S3 as the payload was too large to simply return the list from AWS Lambda.
              </Card.Text>
            </Card.Body>
            <ListGroup className="list-group-flush">
              <ListGroupItem>
                <span>
                  <img src={ReactIcon} alt="react" /> React
                </span>
              </ListGroupItem>
              <ListGroupItem>
                <span>
                  <img src={PythonIcon} alt="python" /> Python
                </span>
              </ListGroupItem>
              <ListGroupItem>
                <span>
                  <img src={MaterialUIIcon} alt="material ui" /> Material UI
                </span>
              </ListGroupItem>
              <ListGroupItem>
                <span>
                  <img src={AWSIcon} alt="aws" /> AWS
                </span>
              </ListGroupItem>
            </ListGroup>
            <Card.Body>
              <Button
                variant="dark"
                style={{ width: "100%" }}
                onClick={() => {
                  window.open("https://www.steam.girard.codes", "_blank");
                }}
              >
                View
              </Button>
            </Card.Body>
          </Card>
        </Row>
        <Row style={{ flexGrow: 0, justifyContent: "center" }}>
          <a href="#contact">
            <DownIcon size={50} color="white" style={{ margin: 50 }} />
          </a>
        </Row>
      </Col>
    </Row>
  );
}
