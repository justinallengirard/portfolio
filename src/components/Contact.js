import React from "react";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import LinkedInImg from "../images/linkedin.png";

export default function Contact(props) {
  return (
    <Row id="contact" style={{ flexGrow: 1, backgroundColor: "#2e2e2e" }}>
      <Col>
        <Row style={{ justifyContent: "center" }}>
          <h1 style={{ color: "white", textAlign: "center", margin: 50 }}>CONTACT</h1>
        </Row>
        <Row style={{ justifyContent: "center" }}>
          <h4 style={{ color: "white", textAlign: "center", marginBottom: 50 }}>JustinAllenGirard@gmail.com</h4>
        </Row>
        <Row style={{ justifyContent: "center" }}>
          <h4 style={{ color: "white", textAlign: "center", marginBottom: 50 }}>
            <img
              src={LinkedInImg}
              alt="linkedin"
              width={80}
              height={80}
              onClick={() => {
                window.open("https://www.linkedin.com/in/justingirard/", "_blank");
              }}
              style={{ cursor: "pointer" }}
            />
          </h4>
        </Row>
      </Col>
    </Row>
  );
}
