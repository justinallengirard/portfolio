import React, { useState } from "react";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { ArrowDownCircleFill as DownIcon, List as NavIcon } from "react-bootstrap-icons";
import Projects from "./Projects.js";
import Resume from "./Resume.js";
import Contact from "./Contact.js";

export default function Home(props) {
  const [showNav, toggleNav] = useState(false);

  return (
    <div>
      <div style={{ position: "fixed", zIndex: 1000, top: 20, left: 20 }}>
        <NavIcon
          size={40}
          onClick={() => {
            toggleNav(!showNav);
          }}
          style={{ backgroundColor: "white", borderRadius: "20%", cursor: "pointer" }}
        ></NavIcon>
        {showNav ? (
          <div style={{ backgroundColor: "white", marginTop: 5, borderRadius: "5%" }}>
            <Nav defaultActiveKey="#home" className="flex-column">
              <Nav.Item>
                <Nav.Link
                  href="#home"
                  onClick={() => {
                    toggleNav(!showNav);
                  }}
                >
                  Home
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link
                  href="#projects"
                  onClick={() => {
                    toggleNav(!showNav);
                  }}
                >
                  Projects
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link
                  href="#resume"
                  onClick={() => {
                    toggleNav(!showNav);
                  }}
                >
                  Resume
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link
                  href="#contact"
                  onClick={() => {
                    toggleNav(!showNav);
                  }}
                >
                  Contact
                </Nav.Link>
              </Nav.Item>
            </Nav>
          </div>
        ) : (
          <div></div>
        )}
      </div>
      <Container
        className="vh-100 d-flex flex-column"
        style={{
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Row id="home" style={{ flexGrow: 1, justifyContent: "center", alignItems: "center", width: "70%" }}>
          <Col style={{ textAlign: "center" }}>
            <h1>Welcome</h1>
            <h6>
              I'm a software developer from Michigan and I'm currently available for employment opportunities. Please take a look around my portfolio and feel
              free to reach out.
              <br></br>
              <br></br> Thanks!
            </h6>
            <h6 style={{ textAlign: "right", marginTop: 30 }}>- Justin Girard</h6>
          </Col>
        </Row>
        <Row style={{ flexGrow: 0 }}>
          <Col>
            <a href="#projects">
              <DownIcon size={50} color="#2e2e2e" style={{ marginBottom: 30 }} />
            </a>
          </Col>
        </Row>
      </Container>
      <Container className="d-flex flex-row">
        <Projects />
      </Container>
      <Container className="d-flex flex-row">
        <Resume />
      </Container>
      <Container className="d-flex flex-row">
        <Contact />
      </Container>
    </div>
  );
}
